<?php if ( ! defined( 'ABSPATH' ) ) exit;
/*
Plugin Name: Main Category Picker
Plugin URI: http://theaveragedev.com/main-category-picker
Description: Allows the setting and then the use of main category and sub-category on a per-post base.
Version: 1.0
Author: theAverageDev, Luca Tumedei
Author URI: http://theaveragedev.com
License: GPL2
*/
if ( ! defined( "ROOT" ) ) {
  define( "ROOT", dirname( __FILE__ ) );
}

class Main_Category_Picker {

  public $_selected_post_types;

  function __construct() {
    $this->_selected_post_types = $this->get_selected_post_types_from_options();
    add_action( 'save_post', array( $this, 'save' ), 10 , 2 );
    add_action( 'add_meta_boxes', array( $this, 'add' ) );
  }

  function get_selected_post_types_from_options() {
    return array( 'post', 'event' , 'page' );
  }

  function save( $post_id, $post ) {
    if ( !isset( $_POST['mcp_class_nonce'] ) || ! wp_verify_nonce( $_POST['mcp_class_nonce'], plugin_basename( __FILE__ ) ) )
      return;

    if ( 'page' == $_POST['post_type'] ) {
      if ( !current_user_can( 'edit_page', $post_id ) )
        return;
    }
    else {
      if ( !current_user_can( 'edit_post', $post_id ) )
        return;
    }

    $this->_update( $post_id, $post );
  }

  function _update( $post_id, $post ) {
    $new_main_parent_c = isset( $_POST['_main_category'] ) ? $_POST['_main_category'] : '';
    $new_main_child_c = isset( $_POST['_main_sub_category'] ) ? $_POST['_main_sub_category'] : '';

    $main_parent_c_key = '_main_category';
    $main_child_c_key = '_main_sub_category';

    $saved_main_parent_c = get_post_meta( $post_id, '_main_category', true );
    $saved_main_child_c = get_post_meta( $post_id, '_main_sub_category', true );

    if ( $new_main_parent_c != '' && $saved_main_parent_c == '' )
      add_post_meta( $post_id, $main_parent_c_key, $new_main_parent_c, true );
    elseif ( $new_main_parent_c != '' && $new_main_parent_c != $saved_main_parent_c )
      update_post_meta( $post_id, $main_parent_c_key, $new_main_parent_c, $saved_main_parent_c );
    elseif ( $new_main_parent_c == '' && $saved_main_parent_c != '' )
      delete_post_meta( $post_id, $main_parent_c_key, $saved_main_parent_c );

    if ( $new_main_child_c != '' && $saved_main_child_c == '' )
      add_post_meta( $post_id, $main_child_c_key, $new_main_child_c, true );
    elseif ( $new_main_child_c != '' && $new_main_child_c != $saved_main_child_c )
      update_post_meta( $post_id, $main_child_c_key, $new_main_child_c, $saved_main_child_c );
    elseif ( $new_main_child_c == '' && $saved_main_child_c != '' )
      delete_post_meta( $post_id, $main_child_c_key, $saved_main_child_c );
  }


  function add() {
    $pages = $this->_selected_post_types;
    if ( is_array( $pages ) )
      foreach ( $pages as $page ) {
        add_meta_box(
          'mcp_meta_box',
          __( 'Main Categories', $domain = 'main-category-picker' ),
          array( $this, 'render' ),
          $page,
          'side'
        );
      }
  }

  function render( $post ) {

    wp_nonce_field( plugin_basename( __FILE__ ), 'mcp_class_nonce' );

    $categories = wp_get_post_categories( $post->ID );
    $parent_categories = array();
    $child_categories = array();
    if ( is_array( $categories ) )
      foreach ( $categories as $category ) {
        $c = get_category( $category );
        $c->parent ? $child_categories[]=$c : $parent_categories[]=$c;
      }
    // returns an empty string if nothing is found
    $main_parent_c = get_post_meta( $post->ID, '_main_category', true );
    $main_child_c = get_post_meta( $post->ID, '_main_sub_category', true );
?>
      <p>Only Categories assigned to the post will appear here.
        Assign some Categories to the post and Update to see them here.</p>
        <?php if ( isset( $main_child_c ) && $main_child_c != '' ) : ?>
        <p>Currently "<?php echo $main_child_c; ?>" is the main Sub-category.</p>
      <?php else :?>
      <p>No main Sub-category selected.</p>
    <?php endif; ?>
    <?php if ( isset( $main_parent_c ) && $main_parent_c != '' ) :  ?>
    <p>Currently "<?php echo $main_parent_c; ?>" is the main Category.</p>
  <?php else : ?>
  <p>No main Category selected.</p>
<?php endif; ?>
<?php if ( count( $child_categories ) >0 ) : ?>
  <div>
   <label for="_main_sub_category"><?php echo __( 'Select the main Sub Category for the $post:', $domain = 'main-category-picker' ) ?></label>
   <div>
    <select name="_main_sub_category" id="_main_sub_category">
     <?php foreach ( $child_categories as $c ) { ?>
     <option value="<?php echo $c->slug; ?>" <?php selected( $main_child_c, $c->slug ); ?>><?php echo $c->name; ?></option>
     <?php } ?>
   </select>
 </div>
</div>
<?php else : ?>
 <p>This post has no Sub-categories assigned to it.</p>
<?php endif; ?>
<?php if ( count( $parent_categories ) >0 ) : ?>
  <div>
   <label for="_main_category"><?php echo __( 'Select the main Category for the $post:', $domain = 'main-category-picker' ) ?></label>
   <div>
    <select name="_main_category" id="_main_category">
     <?php foreach ( $parent_categories as $c ) { ?>
     <option value="<?php echo $c->slug; ?>" <?php selected( $main_parent_c, $c->slug ); ?>><?php echo $c->name; ?></option>
     <?php } ?>
   </select>
 </div>
</div>
<?php else: ?>
 <p>This post has no Categories assigned to it.</p>
<?php endif; ?>
<?php
  }

  public static function get_main_category( $post_id ) {
    $meta_value = get_post_meta( $post_id, $key = '_main_category', true );
    $match = null;
    $cats = wp_get_post_categories( $post_id );
    foreach ( $cats as $cat ) {
      $c = get_category( $cat );
      if ( $c->slug == $meta_value )
        return $c;
      if ( ! $c->parent )
        if ( ! isset( $match ) || $c->term_id > $match->term_id )
          $match = $c;
    }
    return $match;
  }

  public static function get_main_sub_category( $post_id ) {
    $meta_value = get_post_meta( $post_id, $key = '_main_sub_category', true );
    $match = null;
    $cats = wp_get_post_categories( $post_id );
    foreach ( $cats as $cat ) {
      $c = get_category( $cat );
      if ( $c->slug == $meta_value )
        return $c;
      if ( $c->parent )
        if ( ! isset( $match ) || $c->term_id > $match->term_id )
          $match = $c;
    }
    return $match;
  } // end of get_main_sub_category

} // end of class

$GLOBALS['main-category-picker'] = new Main_Category_Picker();

// =============================================================================
// Template Tags
// =============================================================================

function get_main_subcategory() {
  global $post;
  return Main_Category_Picker::get_main_sub_category( $post->ID );
}

function get_main_category() {
  global $post;
  return Main_Category_Picker::get_main_category( $post->ID );
}
/**
 * Echoes the main category for the post
 */
function the_main_category() {
  global $post;
  $cat = Main_Category_Picker::get_main_category( $post->ID );
  $link = get_term_link( $cat );
  if ( is_wp_error( $link ) )
    $link = '';
?>
  <a href="<?php echo $link; ?>">
    <?php echo $cat->name; ?>
  </a>
  <?php
}
/**
 * Echoes the main subcategory for the post
 */
function the_main_subcategory() {
  global $post;
  $cat = Main_Category_Picker::get_main_sub_category( $post->ID );
  $link = get_term_link( $cat );
  if ( is_wp_error( $link ) )
    $link = '';
?>
  <a href="<?php echo $link; ?>">
    <?php
  if ( $cat ) echo $cat->name;
?>
  </a>
  <?php
} ?>
