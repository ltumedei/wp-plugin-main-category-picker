<?php
// Load WordPress test environment
// https://github.com/nb/wordpress-tests
//
// The path to wordpress-tests
$path = '/src/wordpress-tests/includes/bootstrap.php';

// The $path to the root folder
define( "ROOT", dirname(dirname(__FILE__)));

if( file_exists( $path ) ) {
    require_once $path;
} else {
    exit( "Couldn't find path to wordpress-tests/bootstrap.php\n" );
}
