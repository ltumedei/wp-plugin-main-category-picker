<?php

require_once ROOT . '/main_category_picker.php';
require_once ROOT . '/tests/libs/utils.php';
require_once ROOT . '/tests/libs/Term_Factory.php';

class Main_Category_Picker_Test extends WP_UnitTestCase
{

	public $plugin;
	public $factory;

	public function setUp()
	{
		parent::setUp();
		$this->factory = new WP_UnitTest_Factory();
		$this->term_factory = new Term_Factory();
		$this->plugin = $GLOBALS['main-category-picker'];
	}

	function test_has_initialized(){
		$this->assertFalse($this->plugin == null );
	}

	function test_get_selected_post_types_from_options_returns_an_array(){
		$this->assertTrue(is_array($this->plugin->get_selected_post_types_from_options() ) );
	}

	function test_hooks_into_add_meta_boxes(){
		$this->assertTrue(has_filter( 'add_meta_boxes', array($this->plugin, 'add')) != false);
	}

	function test_hooks_into_save_post(){
		$this->assertTrue(has_filter( 'save_post', array($this->plugin, 'save') ) != false);
	}

	function spoof_nonce_fields(){
		wp_nonce_field( $this->plugin->_get_filename, 'mcp_class_nonce' );
	}

	function post_types_provider(){
		return array(
			array(1, array('post'),
				array(2,array('post', 'page')),
				array(4,array('post', 'page', 'foo', 'baz')),
				array(3,array('$page', 'foo', 'bar')),
				array(0, array()),
				array(0, null)
				)
			);
	}

	/**
 	* @dataProvider post_types_provider
 	*/
 	function test_adds_right_number_of_meta_boxes($count, $pages){
 		global $wp_meta_boxes;
 		$this->plugin->_selected_post_types = $pages;
 		$this->plugin->add();
 		$matches = 0;
 		$matches = array_key_count_r('mcp_meta_box', $wp_meta_boxes);
 		$this->assertEquals( $count , $matches );
 	}

 	function test_update_adds_post_meta(){
 		$post_id = $this->factory->post->create();
 		$meta_keys = array( '_main_category' , '_main_sub_category');
 		foreach ($meta_keys as $key) {
 			$_POST[$key] = 'new_foo';
 			$this->plugin->_update($post_id, get_post( $post_id) );
 			// update_post_meta( $post_id, $key, $_POST[$key]);
 			$this->assertEquals('new_foo', get_post_meta( $post_id, $key,true ), 'meta-key is: ' . $key);
 		}
 	}

 	function test_update_updates_post_meta(){
 		$post_id = $this->factory->post->create();
 		$meta_keys = array( '_main_category' , '_main_sub_category');
 		foreach ($meta_keys as $key) {
 			//set a post meta
 			add_post_meta( $post_id, $key, 'saved_foo', true);
 		}
 		foreach ($meta_keys as $key) {
 			$_POST[$key] = 'updated_foo';
 			$this->plugin->_update($post_id, get_post( $post_id) );
 			// update_post_meta( $post_id, $key, $_POST[$key]);
 			$this->assertEquals('updated_foo', get_post_meta( $post_id, $key,true ), 'meta-key is: ' . $key);
 		}
 	}

 	function test_update_deletes_post_meta(){
 		$post_id = $this->factory->post->create();
 		$meta_keys = array( '_main_category' , '_main_sub_category');
 		foreach ( $meta_keys as $key) {
 			add_post_meta( $post_id, $key, 'saved_foo', $unique = true);
 		}
 		foreach ($meta_keys as $key) {
 			$_POST[$key] = '';
 			$this->plugin->_update($post_id, get_post( $post_id) );
 			$this->assertEquals('', get_post_meta( $post->ID, $key,true ), 'meta-key is: ' . $key);
 		}
 	}


 	function test_update_works_for_pages(){
 		$post_id = $this->factory->post->create(array('post_type' => 'page'));
 		$meta_keys = array( '_main_category' , '_main_sub_category');
 		foreach ( $meta_keys as $key) {
 			add_post_meta( $post_id, $key, 'baz', $unique = true);
 		}
 		foreach ($meta_keys as $key) {
 			$_POST[$key] = 'foo';
 			$this->plugin->_update($post_id, get_post( $post_id) );
 			$this->assertEquals('foo', get_post_meta( $post_id, $key,true ), 'meta-key is: ' . $key);
 		}
 	}

 	function tests_categories_are_properly_populted(){
 		global $post;
 		$post_id = $this->factory->post->create();
 		$post = get_post($post_id);
 		$this->term_factory->populate_categories_for_post($post_id);
 		$cat_ids = wp_get_post_categories( $post_id );
 		$parents = array();
 		$children = array();
 		foreach ($cat_ids as $c_id) {
 			$c = get_category( $c_id );
 			if ( $c->taxonomy == 'category'){
 				if ($c->parent) {
 					$children[] = $c;
 				}
 				else {
 					$parents[] = $c;
 				}
 			}
 		}
 		$this->assertTrue(count($parents) == 20 );
 		$this->assertTrue(count($children) == 40 );
 	}

 	function test_get_main_category(){
 		global $post;
 		$post_id = $this->factory->post->create();
 		$post = get_post($post_id);

		// if no main cats and no cats
		// then it returns Uncategorized
 		$this->assertNotNull(Main_Category_Picker::get_main_category($post_id));
 		$this->assertEquals( 'Uncategorized', Main_Category_Picker::get_main_category($post_id)->name);

		// if no main category is found
		// then the function returns the last one
		// and not Uncategorized
 		$this->term_factory->populate_categories_for_post($post_id);
 		$main_cat = $this->term_factory->get_last_main_category($post_id);
 		$this->assertNotNull(Main_Category_Picker::get_main_category($post_id));
 		$this->assertEquals( $main_cat->name, Main_Category_Picker::get_main_category($post_id)->name);

		// if a main category is set
		// but it's not assigned to the post anymore
		// and the post has categories assigned to it
		// then returns the last parent category
		// and not Uncategorized
 		$this->term_factory->populate_categories_for_post($post_id);
 		$main_cat = $this->term_factory->get_last_main_category($post_id);
 		$meta_key = '_main_category';
 		$meta_value = 'foo';
 		add_post_meta( $post_id, $meta_key, $meta_value, true );
 		$this->assertNotNull(Main_Category_Picker::get_main_category($post_id));
 		$this->assertEquals($main_cat->name, Main_Category_Picker::get_main_category($post_id)->name);

 		// if a main category exists
 		// and is an actual category
 		// then return it
 		$this->term_factory->populate_categories_for_post($post_id);
 		$main_cat = $this->term_factory->get_random_main_category($post_id);
 		$meta_value = $main_cat->slug;
 		delete_post_meta( $post_id, $meta_key, 'foo' );
 		add_post_meta( $post_id, $meta_key, $meta_value, true );
 		$this->assertNotNull(Main_Category_Picker::get_main_category($post_id));
 		$this->assertEquals($main_cat->name, Main_Category_Picker::get_main_category($post_id)->name);
 	}

 	function test_get_main_sub_category(){
 		global $post;
 		$post_id = $this->factory->post->create();
 		$post = get_post($post_id);

		// given no main sub category set
		// and no sub categories assigned to the post
		// then it returns null
 		$this->assertNull( Main_Category_Picker::get_main_sub_category($post_id) );

		// given no main sub category set
		// and the post has sub categories assigned to it
		// then the function returns the last sub category
		// and not Uncategorized
 		$this->term_factory->populate_categories_for_post($post_id);
 		$sub_cat = $this->term_factory->get_last_main_category($post_id, true);
 		$this->assertNotNull(Main_Category_Picker::get_main_sub_category($post_id));
 		$this->assertEquals( $sub_cat->name, Main_Category_Picker::get_main_sub_category($post_id)->name);

		// given a main sub category is set
		// and the assigned sub category is not assigned to the post anymore
		// and the post has sub categories assigned to it
		// then returns the last sub category
		// and not Uncategorized
 		$this->term_factory->populate_categories_for_post($post_id);
 		$sub_cat = $this->term_factory->get_last_main_category( $post_id , true );
 		$meta_key = '_main_sub_category';
 		$meta_value = 'foo';
 		add_post_meta( $post_id, $meta_key, $meta_value, true );
 		$this->assertEquals( $sub_cat->name, Main_Category_Picker::get_main_sub_category($post_id)->name);

 		// given a main sub category exists
 		// and is a sub category assigned to the post
 		// then return it
 		$this->term_factory->populate_categories_for_post($post_id);
 		$main_cat = $this->term_factory->get_random_main_category($post_id, true);
 		$meta_value = $main_cat->slug;
 		delete_post_meta( $post_id, $meta_key, 'foo' );
 		add_post_meta( $post_id, $meta_key, $meta_value, true );
 		$this->assertNotNull(Main_Category_Picker::get_main_sub_category($post_id));
 		$this->assertEquals($main_cat->name, Main_Category_Picker::get_main_sub_category($post_id)->name);
 	}

 	public function tearDown()
 	{
         // your code here
 	}
 }
 ?>