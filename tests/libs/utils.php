<?php
function array_key_exists_r($needle, $haystack)
{
	$result = array_key_exists($needle, $haystack);
	if ($result) return $result;
	foreach ($haystack as $v) {
		if (is_array($v)) {
			$result = array_key_exists_r($needle, $v);
		}
		if ($result) return $result;
	}
	return $result;
}

function array_key_count_r($needle, $haystack)
{
	$matches = 0;

	$result = array_key_exists($needle, $haystack);
	if ($result)
		$matches += 1;
	foreach ($haystack as $v) {
		if (is_array($v)) {
			$matches += array_key_count_r($needle, $v);
		}
	}
	return $matches;
}
?>