<?php

if ( defined( 'WORDPRESS_TESTS_ROOT')) {
	require_once WORDPRESS_TESTS_ROOT . '/includes/factory.php';
}

class Term_Factory {

	private $factory = null;

	function __construct (){
		$this->factory = new WP_UnitTest_Factory();
	}

	function _generate_terms($taxonomy = 'category'){
		$parent_ids = $this->factory->term->create_many(20, array('taxonomy'=>$taxonomy));
		$children_ids = array();
		foreach ($parent_ids as $parent_id) {
			$children_ids = array_merge( $this->factory->term->create_many(2, array('taxonomy'=>$taxonomy, 'parent'=>$parent_id)), $children_ids);
		}
		$term_ids = array_merge($parent_ids , $children_ids);
		return $term_ids;
	}

	function populate_categories_for_post($post_id){
		$post_categories = $this->_generate_terms();
		wp_set_post_categories( $post_id, $post_categories );
	}

	function get_last_main_category($post_id , $get_sub = false){
		$post_cats = wp_get_post_categories( $post_id );
		$main_cat = null;
		foreach ($post_cats as $cat_id) {
			$category = get_category( $cat_id );
			if ( $category->parent == $get_sub ) {
				if ( ! isset($main_cat) || $category->term_id > $main_cat->term_id )
					$main_cat = $category;
			}
		}
		return $main_cat;
	}

	function get_random_main_category($post_id, $get_sub = false){
		$post_cats = wp_get_post_categories( $post_id );
		$main_cat = null;
		shuffle($post_cats);
		foreach ( $post_cats as $cat_id) {
			$category = get_category( $cat_id );
			if ( $category->parent == $get_sub ) {
				return $category;
			}
		}
	}
 } // end class
 ?>